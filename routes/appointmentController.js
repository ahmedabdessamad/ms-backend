const express = require("express");
const router = express.Router();
const ObjectID = require("mongoose").Types.ObjectId;
const asyncHandler = require("express-async-handler");
const { Appointment } = require("../models/appointment");
//import Appointment from "../models/appointment";

router.get("/", async (req, res) => {
  const a = await Appointment.find({}).limit(10);

  return res.json(a);
});

router.get("/:id", async (req, res) => {
  const b = await Appointment.findById(req.params.id);
  res.send(b);
});

router.get(
  "/client/:idc",
  asyncHandler(async (req, res) => {
    //res.send(req.params.id)
    client = req.params.idc;
    try {
      const AppointmentByClient = await Appointment.find({
        "clientContactReference.id": client,
      });

      if (!AppointmentByClient) {
        return res.status(400).json({ message: "not found " }).end();
      }

      res.send(AppointmentByClient);
    } catch (e) {
      console.error(e);
      res.status(400).end();
    }
  })
);

module.exports = router;
