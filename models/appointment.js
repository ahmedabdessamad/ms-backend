const mongoose = require("mongoose");

const Appointment = mongoose.model(
  "calendar",
  {
    id: Number,
    appointmentUUID: Number,
    communicationMethod: String,
    dateAdded: Number,
    dateBegin: Number,
    dateEnd: Number,
    candidateReference: {},
    clientContactReference: {
      id: Number,
      firstName: String,
      lastName: String,
      location: String,
    },
    owner: {
      id: Number,
      firstName: String,
      lastName: String,
    },
    childAppointments: {
      total: Number,
      data: { type: [], default: [] },
    },
    subject: String,
    timeZoneID: String,
    isAllDay: Boolean,
    isDeleted: Boolean,
    isPrivate: Boolean,
    jobOrder: {},
    jobSubmission: {},
    lead: {},
    location: String,
    migrateGUID: String,
    notificationMinutes: Number,
    opportunity: {},
    owner: {},
    parentAppointment: {},
    placement: {},
    recurrenceDayBits: Number,
    recurrenceFrequency: Number,
    recurrenceMax: Number,
    recurrenceMonthBits: Number,
    recurrenceStyle: String,
    recurrenceType: String,
    showTimeAs: String,
    subject: String,
    timeZoneID: String,

    type: String,
  },
  "appointment"
);

module.exports = { Appointment };
