const mongoose = require("mongoose");
require('dotenv').config();

mongoose.connect(
  process.env.DATABASE_URL,
  { useNewUrlParser: true, useUnifiedTopology: true },
  (err) => {
    if (!err) console.log("Mongodb Connected");
    else console.log("Connection Failure: " + err);
  }
);
