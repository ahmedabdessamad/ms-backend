const express = require("express");
const app = express();
require("./models/dbConfig");
const bodyParser = require("body-parser");
const appointmentRoute = require("./routes/appointmentController");
const cors = require("cors");

app.use(cors());
app.use(bodyParser.json());
app.use("/API/v1", appointmentRoute);

app.listen(process.env.PORT, () =>
  console.log("Server Started at Port " + process.env.PORT)
);
